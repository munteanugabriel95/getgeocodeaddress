import requests, sys, getopt
import pytest

def getGeocodeByAddr(address):
    try:
        address = address.replace(" ", "+")
        api_key = "AIzaSyDhw9icx3u-Mwvy9gPvQyywdsgkkzdS_As"
        geoMapUrl = f"https://maps.googleapis.com/maps/api/geocode/json?address={address}&key={api_key}"

        response = requests.get(geoMapUrl)

        return response

    except requests.exceptions.HTTPError as errh:
        print("A HTTPError has occured!\n")
        print("Error stacktrace: " + str(errh))
        sys.exit()
    except requests.exceptions.ConnectionError as errc:
        print("A ConnectionError has occured!\n")
        print("Error stacktrace: " + str(errc))
        sys.exit()
    except requests.exceptions.Timeout as errt:
        print("A Timeout has occured!\n")
        print("Error stacktrace: " + str(errt))
        sys.exit()
    except requests.exceptions.RequestException as err:
        print("A RequestException has occured!\n")
        print("Error stacktrace: " + str(err))
        sys.exit()

def main(argumentList):
    #Options
    options = "t:"

    try:
        #Input args
        arguments, values = getopt.getopt(argumentList, options)
        for argument, address in arguments:
            #Accepting only -t arg
            if argument == "-t":
                response = getGeocodeByAddr(address)
                #Returning desired output, only if the API has responded succesfuly and with a response body to use
                if response.status_code == 200:
                    try:
                        #Wasn't sure how to tackle this, did not understood if the requirement is to specifically return the last formal address present in the response
                        respAddress = response.json()["results"][-1]["formatted_address"].split(", ")
                        #Formatting address to match requirement
                        formatterAddr = f'{respAddress[0]},{respAddress[1].replace(" ", ",")},{respAddress[2]}'
                        print(formatterAddr)
                        return formatterAddr

                    except (KeyError, IndexError):
                        #In case of index or key error, printing specific message
                        print("Failed to format the address")
                else:
                    #For any failed http attempt (!200), print warning message
                    print("The HTTP request has failed!\n" +
                          f"Failed response content:\n\t\tStatus code: {response.status_code}\n\t\tResponse: {response.text}")
            else:
                #In case of wrong usage, display helping message
                print('getGeocodeAddress.py -t "Your address here between double commas ("")"')
                return 'getGeocodeAddress.py -t "Your address here between double commas ("")"'

    except getopt.error as err:
        print('getGeocodeAddress.py -t "Your address here between double commas ("")"\n')
        # print(str(err))



if __name__ == "__main__":
   main(sys.argv[1:])
