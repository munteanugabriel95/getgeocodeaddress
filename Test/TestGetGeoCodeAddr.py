import subprocess, unittest
from pathlib import Path


class TestGeoCodeAddr(unittest.TestCase):

    def test_valid_address(self):
        cli = self.get_project_root()
        try:
            result = (subprocess.check_output(['python', f'{cli}\\app\\getGeocodeAddress.py', '-t', '"5 Av. Anatole, Paris, Champ de Mars"'])).decode("utf-8")
            # print(result)
            assert result.strip() == "5 Av. Anatole France,75007,Paris,France"
        except subprocess .CalledProcessError as err:
            print(err)

    def test_invalid_address(self):
        cli = self.get_project_root()
        try:
            result = (subprocess.check_output(['python', f'{cli}\\app\\getGeocodeAddress.py', '-t', '"5 Av. Anatole, Sirap, MARS"'])).decode("utf-8")
            # print(result)
            assert result.strip() == "Failed to format the address"
        except subprocess .CalledProcessError as err:
            print(err)

    def test_invalid_commandLine(self):
        cli = self.get_project_root()
        try:
            result = (subprocess.check_output(['python', f'{cli}\\app\\getGeocodeAddress.py', '-h', '"5 Av. Anatole, Sirap, MARS"'])).decode("utf-8")
            # print(result)
            assert result.strip() == 'getGeocodeAddress.py -t "Your address here between double commas ("")"'
        except subprocess .CalledProcessError as err:
            print(err)

    def test_invalid_commandLine(self):
        cli = self.get_project_root()
        try:
            result = (subprocess.check_output(['python', f'{cli}\\app\\getGeocodeAddress.py', '-h', '"5 Av. Anatole, Sirap, MARS"'])).decode("utf-8")
            # print(result)
            assert result.strip() == 'getGeocodeAddress.py -t "Your address here between double commas ("")"'
        except subprocess .CalledProcessError as err:
            print(err)

    def get_project_root(self) -> Path:
        return Path(__file__).parent.parent


if __name__ == '__main__':
    unittest.main()
